# Template pour Rmarkdown personnalisé pour Bilille

Le template inclut le logo de Bilille et reprend ses couleurs.  
Il est basé sur le format html_document dont certains arguments sont utilisables.

## Installation

Après téléchargement du dépot

```{r}
install.packages('chemin_dans_votre_ordi/template-rmarkdown', repos=NULL)
```
## Utilisation

### Depuis RStudio

À la création du fichier voulu, sélectionner le template :  
File > New File > R Markdown... > From Template > Bilille  
Compléter le nom de l'auteur et le nom du document.

### Manuellement

Dans la section YAML du document Rmarkdown, choisir comme format de sortie templatebilille::bilille_report.

```{r}
---
title: Exemple
author: John Doe
date: "`r format(Sys.time(), '%d %B %Y')`"
output:
  templatebilille::bilille_report
---
```

## Paramètres

Les paramètres par défaut du template sont ([pour plus de détails sur les paramètres](https://bookdown.org/yihui/rmarkdown/html-document.html)) :

```{r}
output:
  templatebilille::bilille_report:
    toc: true
    toc_depth: 4
    toc_float: true 
    number_sections: true
    code_folding: hide
    highlight: textmate
```
Il est possible de choisir une des autres options prévues par le format html_document pour toc_depth, code_folding et highlight.
